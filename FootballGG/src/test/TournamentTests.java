import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

public class TournamentTests 
{
	
	@Test
	public void creationAndGetNameTest()
	{
		String testName = "thisIsAtest";
		String expectedString = "thisIsAtest";
		
		Tournament testTournament = new Tournament(testName); 
		
		assertEquals(testTournament.getName() , expectedString);
	}

}
