import org.junit.jupiter.api.Test;

public class MatchTests {
	
	@SuppressWarnings("unused")
	@Test
	public void creationTest()
	{
		String testName = "thisIsAtest";
		String awayName = "away";
		String homeName = "home";
		String startTime = "13:37";
		String kickOffTime = "10:01";
		int homeScore = 1;
		int guestScore = 0;
		Status status = Status.finished;
		
		Tournament testTournament = new Tournament(testName); 
		Team testHomeTeam = new Team(homeName); 
		Team testAwayTeam = new Team(awayName); 

		
		Match testMatch = new Match(testHomeTeam, testAwayTeam, startTime, kickOffTime, testTournament, homeScore, guestScore, status);
		
	}


}
