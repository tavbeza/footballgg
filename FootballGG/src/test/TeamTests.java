import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

public class TeamTests
{
	
	@Test
	public void creationAndGetNameTest()
	{
		String testName = "thisIsAtest";
		String expectedString = "thisIsAtest";
		
		Team testTournament = new Team(testName); 
		
		assertEquals(testTournament.getName() , expectedString);
	}

}
