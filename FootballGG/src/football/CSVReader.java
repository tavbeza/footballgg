
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CSVReader {
    public static final String UTF8_BOM = "\uFEFF";

    public static void main(String[] args) {
        String csvFile = new File("").getAbsolutePath() + "\\src\\resources\\input.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] matchFromFile = line.split(cvsSplitBy);

                Team home = new Team(matchFromFile[0].replaceAll(UTF8_BOM, ""));
                Team guest = new Team(matchFromFile[1]);
                Tournament tournament = new Tournament(matchFromFile[4]);
                Match match = new Match(home, guest, matchFromFile[2], matchFromFile[3], tournament, Integer.parseInt(matchFromFile[5]), Integer.parseInt(matchFromFile[6]), Status.valueOf(matchFromFile[7]));

                System.out.println(match);
//                System.out.println("Home: " + matchFromFile[0] + " , Origin: " + matchFromFile[1] + ", Date: " + matchFromFile[2] + ", Hour: " + matchFromFile[3] + ", Tournament: " + matchFromFile[4] + ", Score: " + matchFromFile[5] + "-" + matchFromFile[6] + ", Status: " + matchFromFile[7]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
