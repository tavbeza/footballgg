import java.time.LocalTime;
import java.util.Date;

public class Match {
    private Team homeTeam;
    private Team guestTeam;
    private String startDate;
    private String kickOffTime;
    private int homeScore;
    private int guestScore;
    private Tournament tournament;
    private Status status;

    public Match(Team homeTeam, Team guestTeam, String startDate, String kickOffTime, Tournament tournament, int homeScore, int guestScore, Status status) {
        this.homeTeam = homeTeam;
        this.guestTeam = guestTeam;
        this.startDate = startDate;
        this.kickOffTime = kickOffTime;
        this.homeScore = homeScore;
        this.guestScore = guestScore;
        this.tournament = tournament;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Match{" +
                "homeTeam=" + homeTeam +
                ", guestTeam=" + guestTeam +
                ", startDate='" + startDate + '\'' +
                ", kickOffTime='" + kickOffTime + '\'' +
                ", homeScore=" + homeScore +
                ", guestScore=" + guestScore +
                ", tournament=" + tournament +
                ", status=" + status +
                '}';
    }
}
