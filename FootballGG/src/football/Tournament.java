
public class Tournament {
    private static int id = 0;
    private int myID;
    private String name;

    Tournament(String name) {
        this.myID = ++id;
        this.name = name;
    }

    public int getMyID() {
        return myID;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "myID=" + myID +
                ", name='" + name + '\'' +
                '}';
    }
}
