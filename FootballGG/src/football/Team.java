
public class Team {
    private String id;
    private String name;

    Team(String name) {
        this.id = name.substring(0, 3).toUpperCase();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
