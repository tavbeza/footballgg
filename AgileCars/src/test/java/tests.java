import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

public class tests {

	@Test
	public void createUltimateCar() {
		new UltimateCar("test" , 0);
	}
	
	@Test
	public void createCarCompositionArray() {
		
		ArrayList<CarComposition> cars = new ArrayList<CarComposition>();
		cars.add(new CarComposition( new UltimateCar("ult " , 6)));
		cars.add(new CarComposition( new SunCar("sun " , 66)));
		cars.add(new CarComposition( new ElectricCar("electric " , 666)));
		
		for (CarComposition car : cars)
			car.carInfo();
	}
	
	@Test
	public void createCarsArray() {
		
		ArrayList<Car> cars = new ArrayList<Car>();
		cars.add(new UltimateCar("ult " , 6));
		cars.add(new SunCar("sun " , 66));
		cars.add(new ElectricCar("electric " , 666));
		
		for (Car car : cars)
			car.carInfo();
	}
	
	@Test
	public void createElectrifiedCarsArray() {
		
		ArrayList<Car> cars = new ArrayList<Car>();
		cars.add(new SunCar("sun1 " , 66));
		cars.add(new ElectricCar("electric1 " , 666));
		cars.add(new SunCar("sun2 " , 66));
		cars.add(new ElectricCar("electric2 " , 666));
		
		for (Car car : cars)
			car.carInfo();
	}
	
	@Test
	public void createUltimateCarsArray() {
		
		ArrayList<Car> cars = new ArrayList<Car>();
		cars.add(new UltimateCar("ult1 " , 6));
		cars.add(new UltimateCar("ult2 " , 6));
		cars.add(new UltimateCar("ult3 " , 6));
		
		for (Car car : cars)
			car.carInfo();
	}
	
	@Test
	public void createSunCarsArray() {
		
		ArrayList<Car> cars = new ArrayList<Car>();
		cars.add(new SunCar("sun1 " , 66));
		cars.add(new SunCar("sun2 " , 66));
		cars.add(new SunCar("sun3 " , 66));
		
		for (Car car : cars)
			car.carInfo();
	}


}
