#ifndef SunCar_h
#define SunCar_h

#include "ElectrifiedCar.h"

class SunCar : public virtual ElectrifiedCar 
{
public:
	SunCar(string name, int km, double energy, bool sunroof);

	void carInfo();

	void AvgSun();
};

#endif // !SunCar_h
