#ifndef UltimateCar_h
#define UltimateCar_h

#include "ElectricCar.h"
#include "SunCar.h"
#include "GasolineCar.h"

class UltimateCar : public ElectricCar, public SunCar, public GasolineCar 
{
public:
	UltimateCar(string name, int km, double energy, bool sunroof, int year);

	void carInfo();
};

#endif // !UltimateCar_h