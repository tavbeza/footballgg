#include "UltimateCar.h"

UltimateCar::UltimateCar(string name, int km, double energy, bool sunroof, int year)
	: Car(name, km), ElectrifiedCar(name, km, energy, sunroof), GasolineCar(name, km, year)
	, ElectricCar(name, km, energy, sunroof), SunCar(name, km, energy, sunroof) {}

void UltimateCar::carInfo()
{
	cout << "Type: Ultimate Car" << endl;
	Car::carInfo();
	ElectrifiedCar::carInfo();
	cout << "Year: " << year << endl;
}