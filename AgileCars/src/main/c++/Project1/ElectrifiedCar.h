#ifndef ElectrifiedCar_h
#define ElectrifiedCar_h

#include "Car.h"

class ElectrifiedCar : virtual public Car 
{
public:
	double energy;
	bool sunroof;

	ElectrifiedCar(string name, int km, double energy, bool sunroof);

	virtual void carInfo();
};

#endif // !ElectrifiedCar_h
