#include "ElectrifiedCar.h"

ElectrifiedCar::ElectrifiedCar(string name, int km, double energy, bool sunroof) : Car(name, km), energy(energy), sunroof(sunroof) {}

void ElectrifiedCar::carInfo() 
{
	cout << "Energy: " << energy << endl << "Have Sunroof? " << (sunroof ? "Yes" : "No") << endl;
}