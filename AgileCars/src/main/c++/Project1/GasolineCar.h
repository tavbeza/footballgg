#ifndef GasolineCar_h
#define GasolineCar_h

#include "Car.h"

class GasolineCar : public virtual Car 
{
public:
	int year;

	GasolineCar(string name, int km, int year);

	virtual void carInfo();

	void AvgGas();
};

#endif // !GasolineCar_hs
