#include "GasolineCar.h"

GasolineCar::GasolineCar(string name, int km, int year) : Car(name, km), year(year) {}

void GasolineCar::carInfo()
{
	cout << "Type: Gasoline Car" << endl;
	Car::carInfo();
	cout << "Year: " << year << endl;
}


void GasolineCar::AvgGas() 
{
	double avg = km / (2019 - year);
	cout << "Average Gas: " << avg << endl;
}
