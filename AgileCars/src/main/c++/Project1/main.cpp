#include "UltimateCar.h"
#include <stdio.h>
#include <iostream>


UltimateCar* newUltimateCar();
SunCar* newSunCar();
ElectricCar* newElectricCar();
GasolineCar* newGasolineCar();

int main()
{
	Car* cars[4];

	cars[0] = newUltimateCar();
	cout << endl;
	cars[1] = newSunCar();
	cout << endl;
	cars[2] = newElectricCar();
	cout << endl;
	cars[3] = newGasolineCar();
	cout << endl;


	for (int i = 0; i < 4; i++)
	{
		cars[i]->carInfo();
		cout << endl;
	}

	cout << "---------------------------------" <<endl;

	ElectrifiedCar* electrifiedCars[2];
	electrifiedCars[0] = newElectricCar();
	cout << endl;
	electrifiedCars[1] = newSunCar();
	cout << endl;
	

	for (int i = 0; i < 2; i++)
	{
		electrifiedCars[i]->carInfo();
		cout << endl;
	}

	cout << "---------------------------------" << endl;

	ElectricCar* electricCars[2];
	electricCars[0] = newElectricCar();
	cout << endl;
	electricCars[1] = newElectricCar();
	cout << endl;


	for (int i = 0; i < 2; i++)
	{
		electricCars[i]->carInfo();
		cout << endl;
	}

	cout << "---------------------------------" << endl;

	SunCar* sunCars[2];
	sunCars[0] = newSunCar();
	cout << endl;
	sunCars[1] = newSunCar();
	cout << endl;


	for (int i = 0; i < 2; i++)
	{
		sunCars[i]->carInfo();
		cout << endl;
	}

	cout << "---------------------------------" << endl;

	UltimateCar* ultimateCars[2];
	ultimateCars[0] = newUltimateCar();
	cout << endl;
	ultimateCars[1] = newUltimateCar();
	cout << endl;


	for (int i = 0; i < 2; i++)
	{
		ultimateCars[i]->carInfo();
		cout << endl;
	}
}

UltimateCar* newUltimateCar() 
{
	UltimateCar* ultCar = new UltimateCar("Aoudi", 55000 + rand(), 150, true, 2018);

	ultCar->carInfo();
	ultCar->AvgElect();
	ultCar->AvgGas();
	ultCar->AvgSun();

	return ultCar;
}

SunCar* newSunCar()
{
	SunCar* sunCar = new SunCar("Marcedes", 20000 + rand(), 100, false);

	sunCar->carInfo();
	sunCar->AvgSun();

	return sunCar;
}

ElectricCar* newElectricCar()
{
	ElectricCar* electricCar = new ElectricCar("Honda", 34000 + rand(), 85, true);

	electricCar->carInfo();
	electricCar->AvgElect();

	return electricCar;
}

GasolineCar* newGasolineCar() 
{
	GasolineCar* gasolineCar = new GasolineCar("Hyoundai", 98000 + rand(), 2002);

	gasolineCar->carInfo();
	gasolineCar->AvgGas();

	return gasolineCar;
}
