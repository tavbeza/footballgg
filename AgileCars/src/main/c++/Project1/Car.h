#ifndef Car_h
#define Car_h

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

class Car 
{
public:
	string name;
	int km;

	Car(string name, int km);

	virtual void carInfo();
};

#endif // !Car_h


