#ifndef ElectricCar_h
#define ElectricCar_h

#include "ElectrifiedCar.h"

class ElectricCar : public virtual ElectrifiedCar 
{
public:
	ElectricCar(string name, int km, double energy, bool sunroof);

	void carInfo();

	void AvgElect();
};

#endif // !ElectrifiedCar_h
