#include "SunCar.h"

SunCar::SunCar(string name, int km, double energy, bool sunroof) : Car(name, km), ElectrifiedCar(name, km, energy, sunroof) {}

void SunCar::carInfo()
{
	cout << "Type: Sun Car" << endl;
	Car::carInfo();
	ElectrifiedCar::carInfo();
}

void SunCar::AvgSun() 
{
	double avg = energy * 2100.8;
	cout << "Average Sun: " << avg << endl;
}