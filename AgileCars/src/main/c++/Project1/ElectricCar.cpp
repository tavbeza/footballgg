#include "ElectricCar.h"

ElectricCar::ElectricCar(string name, int km, double energy, bool sunroof) : Car(name, km), ElectrifiedCar(name, km, energy, sunroof) {}

void ElectricCar::carInfo()
{
	cout << "Type: Electric Car" << endl;
	Car::carInfo();
	ElectrifiedCar::carInfo();
}

void ElectricCar::AvgElect() 
{
	double avg = km * energy;
	cout << "Average Electricity: " << avg << endl;
}