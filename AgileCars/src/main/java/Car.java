public class Car {
    protected String name;
    protected int km;
    
    public Car()
    {
    	this.name = "";
    	this.km = 0;
    }
    
    public Car(String name, int km)
    {
    	this.name = name;
    	this.km = km;
    }

    void carInfo() {
        System.out.println("Name:" + name + ", KM: " + km);
    }

}
