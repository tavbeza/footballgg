
public class CarComposition extends Car {

	Car obj = null;

	public CarComposition(Car obj) {
		this.obj = obj;
	}

	void carInfo() {
		//System.out.println("Name:" + obj.name + ", KM: " + obj.km);
		obj.carInfo();
	}

}
