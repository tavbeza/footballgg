import java.io.PrintWriter;

public class Matrix {

	private int[][] theMatrix;
	private int dimention;

	public Matrix(int dimention) {
		this.dimention = dimention;
		this.theMatrix = new int[dimention][dimention];

		for (int i = 0; i < dimention; i++) {
			for (int j = 0; j < dimention; j++) {
				int value = (int) (Math.random() * 10) + 1;
				this.theMatrix[i][j] = value;
			}
		}

	}

	public Matrix(int[][] matrix) {
		this.theMatrix = matrix;
		this.dimention = matrix[0].length;
	}

	public Matrix multiply(Matrix matrix) {
		int[][] resultMatrix = new int[dimention][dimention];
		int sum = 0;
		for (int p = 0; p < dimention; p++) {
			for (int q = 0; q < dimention; q++) {
				for (int r = 0; r < dimention; r++) {
					sum = sum + this.theMatrix[p][r] * matrix.theMatrix[r][q];
				}

				resultMatrix[p][q] = sum;
			}
		}

		return new Matrix(resultMatrix);
	}
	
	public void writeToFile(String fileName) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fileName + ".txt", "UTF-8");
			for(int i = 0; i < dimention; i++) {
				for(int j = 0; j < dimention; j++) {
					writer.print(theMatrix[i][j]);
				}
				writer.println();
			}
			writer.close();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void printMatrix() {
		System.out.println("\n");
		for (int i = 0; i < dimention; i++) {
			for (int j = 0; j < dimention; j++) {
				System.out.print(this.theMatrix[i][j] + "\t");
			}

			System.out.print("\n");
		}
	}

}