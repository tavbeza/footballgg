import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadService implements ExecutorService {

	private final Lock lock = new ReentrantLock();

	private final Condition termination = lock.newCondition();

	private int runningTasks;
	private boolean shutdown = false;
	
	public ThreadService (int numOfThreads) {
		this.runningTasks = numOfThreads;
		
	}

	@Override
	public void execute(Runnable command) {
		startTask();
		try {
			command.run();
		} finally {
			endTask();
		}

	}
	
	private void startTask() {
		lock.lock();
		try {
			if (isShutdown()) {
				// throw new RejectedExecutionException("Executor already shutdown");
			}
			runningTasks++;
		} finally {
			lock.unlock();
		}
	}

	/**
	 * Decrements the running task count.
	 */
	private void endTask() {
		lock.lock();
		try {
			runningTasks--;
			if (isTerminated()) {
				termination.signalAll();
			}
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public boolean isTerminated() {
		lock.lock();
		try {
			return shutdown && runningTasks == 0;
		} finally {
			lock.unlock();
		}
	}

	@Override
	public void shutdown() {
		lock.lock();
		try {
			shutdown = true;
		} finally {
			lock.unlock();
		}

	}

	@Override
	public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
			throws InterruptedException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException, TimeoutException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isShutdown() {
		lock.lock();
		try {
			return shutdown;
		} finally {
			lock.unlock();
		}

	}

	

	@Override
	public List<Runnable> shutdownNow() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> Future<T> submit(Callable<T> task) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<?> submit(Runnable task) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> Future<T> submit(Runnable task, T result) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
