import java.util.concurrent.LinkedBlockingQueue;

public class MyThreadPool {
	private final int threadsAmount;
	private final PoolWorker[] threads;
	private final LinkedBlockingQueue<Runnable> queue;
	private Boolean isDone = false;

	public MyThreadPool(int threadsAmount) {
		this.threadsAmount = threadsAmount;
		queue = new LinkedBlockingQueue<Runnable>();
		threads = new PoolWorker[threadsAmount];

		for (int i = 0; i < threadsAmount; i++) {
			threads[i] = new PoolWorker();
			threads[i].start();
		}
	}

	public void execute(Runnable task) {
		// synchronized (queue) {
		queue.add(task);
		// queue.notify();
		// }
	}

	public void done() {
		for (int i = 0; i < threadsAmount; i++) {
			threads[i].done();
		}

		isDone = true;
	}

	public Boolean getIsDone() {
		return isDone;
	}

	public boolean isDoneWithCurrentWork() {
		boolean isDoneWithCurrentWork = true;
		for (int i = 0; i < threadsAmount; i++) {
			isDoneWithCurrentWork = isDoneWithCurrentWork & threads[i].getIsDone();
		}
		return isDoneWithCurrentWork;
	}

	private class PoolWorker extends Thread {

		Boolean dontStop = true;
		Boolean isDone = false;

		public void run() {
			while (dontStop == true) {
				Runnable task = null;

				try {
					task = (Runnable) queue.take();

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				isDone = false;
				try {
					task.run();
				} catch (RuntimeException e) {
					System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
				}

				isDone = true;
			}
		}

		public void done() {
			dontStop = false;
		}

		public boolean getIsDone() {
			return isDone;
		}

	}

}