import java.util.Scanner;

public class MatrixMultThread implements Runnable {
	private int numOfMatrices;
	private int dimention;
	private Matrix resultMatrix;
	private String ID;

	public MatrixMultThread(int numOfMatrices, int dimention) {
		this.numOfMatrices = numOfMatrices;
		this.dimention = dimention;
	}

	@Override
	public void run() {
		this.ID = Thread.currentThread().getName();
		System.out.println("Executing " + ID);
		Matrix[] matrices = new Matrix[numOfMatrices];
		for (int i = 0; i < numOfMatrices; i++) {
			matrices[i] = new Matrix(dimention);
		}

		resultMatrix = matrices[0];
		for (int i = 1; i < matrices.length; i++) {
			resultMatrix = matrices[i].multiply(resultMatrix);
		}

		printResultS(resultMatrix);
	}

	private static synchronized void printResultS(Matrix resultMatrix) 
	{
		System.out.println("------------------------------------------------------");
		System.out.println(Thread.currentThread().getName() + " Final matrix is:");
		resultMatrix.printMatrix();
	}

}