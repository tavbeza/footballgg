import java.util.Scanner;

public class MatrixMultThreadPool {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int numOfThreads;
		do {
			System.out.println("How many threads you want to create additionally? (between 2 to 20) :");
			numOfThreads = Integer.parseInt(sc.next());
			if (numOfThreads < 2 || numOfThreads > 20) {
				System.out.println("Invalid Input!");
			} else {
				break;
			}
		} while (true);

		MyThreadPool myThreadPool = new MyThreadPool(numOfThreads);
		MatrixMultThread[] threadArray = new MatrixMultThread[numOfThreads];

		while (myThreadPool.getIsDone() == false) {
			
			try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}//1000ms = 1s
			
			for (int i = 0; i < numOfThreads; i++) {
				System.out.println("*****************************");
				System.out.println("Enter number of matrices:");
				int numOfMatrices = Integer.parseInt(sc.next());
				System.out.println("Enter dimention:");
				int dimention = Integer.parseInt(sc.next());
				threadArray[i] = new MatrixMultThread(numOfMatrices, dimention);
			}

			for (int i = 0; i < numOfThreads; i++) {
				myThreadPool.execute(threadArray[i]);
			}
			
			while(myThreadPool.isDoneWithCurrentWork() == false) {};
			 
		}
			//sc.close();
			//System.out.println("All threads are executed.");
		
	}

}
