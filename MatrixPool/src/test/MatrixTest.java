
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class MatrixTest {
    int[][] matrixValues = {{5, 4}, {3, 2}};
    Matrix matrix = new Matrix(matrixValues);

    @Test
    public void createIllegalMatrix() {
        int illegalValue = -3;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Matrix(illegalValue);
        });
    }

    @Test
    public void multiply() {
        int[][] valuesToMultiply = {{3, 3}, {2, 2}};
        Matrix matrixToMultiply = new Matrix(valuesToMultiply);

        int[][] expectedValues = {{23, 23}, {13, 13}};
        Matrix expectedMatrix = new Matrix(expectedValues);

        Matrix afterMult = matrix.multiply(matrixToMultiply);
        assertEquals(expectedMatrix, afterMult);
    }
}
